# 创建工程
django-admin startproject project_name

# 添加应用
python manage.py startapp app_name

# 启动应用
python manage.py runserver 0.0.0.0:8000

##############数据库###############
# 1. 创建更改的文件
python manage.py makemigrations
# 2. 将生成的py文件应用到数据库
python manage.py migrate

# 导出数据 导入数据
python manage.py dumpdata appname > appname.json
python manage.py loaddata appname.json

# 创建超级管理员
python manage.py createsuperuser

# 修改 用户密码可以用：
python manage.py changepassword username
