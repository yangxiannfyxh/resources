sudo apt-get update 
sudo apt-get install -y curl 
sudo mkdir -p /usr/local/ 
curl https://dl.google.com/go/go1.11.linux-amd64.tar.gz \
  | sudo tar -zxC /usr/local/
sudo sh -c "echo 'export GOPATH=/opt/go  
export GOROOT=/usr/local/go
export PATH=\$PATH:\${GOROOT}/bin' >> /etc/profile"
