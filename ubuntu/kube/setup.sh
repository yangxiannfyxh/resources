#!/bin/bash
role="$1"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_URL=https://gitlab.com/yangxiannfyxh/resources/raw/master/ubuntu/kube

tmp=${DIR}/tmp
sudo mkdir -p ${tmp}
all_deb='cri-tools_1.12.0-00_amd64.deb
kubeadm_1.11.3-00_amd64.deb
kubectl_1.11.3-00_amd64.deb
kubelet_1.11.3-00_amd64.deb
kubernetes-cni_0.6.0-00_amd64.deb'
for deb in ${all_deb} ; do
    name=${tmp}/${deb}
    if [ ! -f ${name} ] ; then
        sudo wget -O ${name} ${BASE_URL}/deb/${deb}
    fi
done

sudo dpkg -i ${tmp}/*.deb
sudo apt-get install -f -y
#sudo rm -f ${tmp}/*.deb

images='kube-apiserver-amd64:v1.11.3
kube-controller-manager-amd64:v1.11.3
kube-scheduler-amd64:v1.11.3
kube-proxy-amd64:v1.11.3
kubernetes-dashboard-amd64:v1.10.0
pause:3.1
etcd-amd64:3.2.18
coredns:1.1.3'
r=registry.cn-hangzhou.aliyuncs.com/google_containers/
k=k8s.gcr.io/
for image in ${images} ; do
    docker pull ${r}${image}
    docker tag ${r}${image} ${k}${image}
    docker rmi ${r}${image}
done
source <(kubectl completion bash)
echo "source <(kubectl completion bash)" >> ~/.bashrc
if [ "${role}" == "master" ] ; then
    sudo wget -O /etc/kubernetes/kubeadm.conf ${BASE_URL}/config/kubeadm.conf
    sudo kubeadm init --config /etc/kubernetes/kubeadm.conf
    sudo mkdir -p ~/.kube
    sudo cp /etc/kubernetes/admin.conf ~/.kube/config

    kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

    kubectl apply -f ${BASE_URL}/yaml/nfs.yaml
    kubectl apply -f ${BASE_URL}/yaml/redis.yaml
    kubectl apply -f ${BASE_URL}/yaml/registry.yaml

    kubectl apply -f ${BASE_URL}/yaml/kubernetes-dashboard.yaml
    kubectl apply -f ${BASE_URL}/yaml/dashboard-admin.yaml

    kubeadm token create --print-join-command
fi