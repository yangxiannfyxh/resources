#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
sudo cp ${DIR}/.bashrc ~/
sudo cp -r ${DIR}/bin ~/
sudo apt-get install -y thefuck python-pip virtualenv ansible
sudo pip install --upgrade pip ansible
virtualenv ~/venv
