#!/bin/bash
export base_url=https://gitlab.com/yangxiannfyxh/resources/raw/master/ubuntu/
sudo -E wget ${base_url}config/sources.list -O /etc/apt/sources.list
sudo apt-get update && apt-get install -y software-properties-common
sudo add-apt-repository -y ppa:nginx/stable
sudo apt-get update && sudo apt-get install -y nginx 
sudo sed -i s'/access_log.*//'g /etc/nginx/nginx.conf
sudo sed -i s'/error_log.*//'g /etc/nginx/nginx.conf

export www_dir=/opt/nfyxhan/www/
sudo -E mkdir -p ${www_dir}

sudo -E wget ${base_url}nginx/robots.txt -O ${www_dir}robots.txt
sudo -E wget ${base_url}nginx/nginx.conf -O /etc/nginx/conf.d/nginx.conf
sudo -E sh -c "echo cVK2I2qxsDpNTo1R > ${www_dir}MP_verify_cVK2I2qxsDpNTo1R.txt"