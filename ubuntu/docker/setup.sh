#!/usr/bin/env bash
sudo apt-get install -y apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
sudo apt-get install -y docker.io

sudo tee /etc/docker/daemon.json <<-'EOF'
 {
   "insecure-registries": ["nfyxhan.com:5000"],
   "registry-mirrors": ["https://e8id4p4w.mirror.aliyuncs.com"]
 }
EOF
sudo service docker restart